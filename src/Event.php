<?php

namespace Empatix\Analytics;

class Event
{
    public $value;
    public $label;
    public $action;
    public $category;

    public function __construct(
        $category,
        $action,
        $value,
        $label = ''
    ) {
        $this->value = $value;
        $this->label = $label;
        $this->action = $action;
        $this->category = $category;
    }
}
