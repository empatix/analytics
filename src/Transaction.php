<?php

namespace Empatix\Analytics;

class Transaction
{
    public $id;
    public $tax;
    public $revenue;
    public $shipping;
    public $affiliation;
    public $currency = 'NOK';

    public function __construct(
        $id,
        $affiliation,
        $revenue,
        $shipping,
        $tax,
        $currency = 'NOK'
    ) {
        $this->id = $id;
        $this->tax = $tax;
        $this->revenue = $revenue;
        $this->shipping = $shipping;
        $this->currency = $currency;
        $this->affiliation = $affiliation;
    }
}
