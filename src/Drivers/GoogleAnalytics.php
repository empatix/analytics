<?php

namespace Empatix\Analytics\Drivers;

use Exception;
use GuzzleHttp\Client;

class GoogleAnalytics extends Driver
{
    protected $client;
    protected $url = 'http://www.google-analytics.com/collect';

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function handle($operation)
    {
        $form = $this->form($operation);

        try {
            $this->client->post($this->url, ['form_params' => $form]);

            $this->registerOperation(true, $form);
        } catch (Exception $e) {
            $this->registerOperation(false, $form);
        }

        return $this;
    }

    protected function form($operation)
    {
        return array_merge([
            'v' => '1',
            'cid' => 555,
            'tid' => $this->config['id'],
            't' => $this->type($operation),
        ], $this->attributes($operation));
    }

    protected function attributesForPageview($pageview)
    {
        return [
            'dh' => $pageview->hostname,
            'dp' => $pageview->page,
            'dt' => $pageview->title,
        ];
    }

    protected function attributesForEvent($event)
    {
        return [
            'ec' => $event->category,
            'ea' => $event->action,
            'el' => $event->label,
            'ev' => $event->value,
        ];
    }

    protected function attributesForItem($item)
    {
        return [
            'in' => $item->name,
            'ip' => $item->price,
            'iq' => $item->quantity,
            'ic' => $item->sku,
            'iv' => $item->category,
            'cu' => $item->currency,
        ];
    }

    protected function attributesForTransaction($transaction)
    {
        return [
            'ti' => $transaction->id,
            'ta' => $transaction->affiliation,
            'tr' => $transaction->revenue,
            'ts' => $transaction->shipping,
            'tt' => $transaction->tax,
            'cu' => $transaction->currency,
        ];
    }
}
