<?php

namespace Empatix\Analytics\Drivers;

use InvalidArgumentException;
use Illuminate\Support\Arr;

abstract class Driver
{
    protected $config = [];
    protected $lastOperation = ['success' => false, 'form_params' => [],];

    public function config($config)
    {
        $this->config = $config;

        return $this;
    }

    public function lastOperation()
    {
        return $this->lastOperation;
    }

    protected function type($operation)
    {
        return strtolower(Arr::last(explode('\\', get_class($operation))));
    }

    protected function attributes($operation)
    {
        if (method_exists($this, $method = sprintf('attributesFor%s', ucfirst($type = $this->type($operation))))) {
            return call_user_func_array([$this, $method], [$operation]);
        }

        throw new InvalidArgumentException("The operation with type {$type} is not available.");
    }

    protected function registerOperation($success, $form_params = [])
    {
        $this->lastOperation = compact('success', 'form_params');
    }

    abstract public function handle($operation);
}
