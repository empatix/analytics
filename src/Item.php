<?php

namespace Empatix\Analytics;

class Item
{
    public $sku;
    public $name;
    public $price;
    public $quantity;
    public $category;
    public $transactionId;
    public $currency = 'NOK';

    public function __construct(
        $transactionId,
        $name,
        $price,
        $quantity,
        $sku,
        $category,
        $currency = 'NOK'
    ) {
        $this->sku = $sku;
        $this->name = $name;
        $this->price = $price;
        $this->quantity = $quantity;
        $this->category = $category;
        $this->currency = $currency;
        $this->transactionId = $transactionId;
    }
}
