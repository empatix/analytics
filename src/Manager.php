<?php

namespace Empatix\Analytics;

use Illuminate\Support\Arr;

class Manager
{
    protected $config = [];

    public function __construct($config = [])
    {
        $this->config($config);
    }

    public function register($operation, $provider = null)
    {
        return $this->provider($provider)->handle($operation);
    }

    public function config($config = null)
    {
        if (is_null($config)) {
            return $this->config;
        }

        $this->config = array_replace_recursive($this->config, $config);

        return $this;
    }

    public function provider($provider = null)
    {
        $provider = $provider ?? $this->config['default'];
        $config = Arr::get($this->config, "providers.{$provider}");
        $driver = Arr::pull($config, 'driver');

        return app($driver)->config($config);
    }
}
