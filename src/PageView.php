<?php

namespace Empatix\Analytics;

class PageView
{
    public $page;
    public $title;
    public $hostname;

    public function __construct($hostname, $page, $title)
    {
        $this->page = $page;
        $this->title = $title;
        $this->hostname = $hostname;
    }
}
