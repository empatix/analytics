# Changelog

All notable changes to `Analytics` will be documented in this file.

## Version 0.1

### Added
- Support for Google Analytics Items.
- Support for Google Analytics Transactions.
