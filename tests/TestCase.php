<?php

namespace Empatix\Analytics\Tests;

abstract class TestCase extends \Orchestra\Testbench\TestCase
{
    protected function getPackageProviders($app)
    {
        return [\Empatix\Analytics\AnalyticsServiceProvider::class];
    }

    protected function getEnvironmentSetUp($app)
    {
        // $app['config']->set('services', $this->config());
    }
}
