<?php

namespace Empatix\Analytics\Tests;

use Empatix\Analytics\Item;
use Empatix\Analytics\Event;
use Empatix\Analytics\PageView;
use Empatix\Analytics\Transaction;
use Empatix\Analytics\Drivers\Driver;
use Empatix\Analytics\Drivers\GoogleAnalytics;

class GoogleAnalyticsTest extends TestCase
{
    /** @test */
    public function it_extends_driver()
    {
        $this->assertInstanceOf(Driver::class, app(GoogleAnalytics::class));
    }

    /** @test */
    public function it_handles_pageviews()
    {
        $pageview = new PageView('example.com', '/home', 'homepage');

        $provider = app(GoogleAnalytics::class)
            ->config(['id' => 'testing'])
            ->handle($pageview);

        $this->assertEquals($provider->lastOperation(), [
            'success' => true,
            'form_params' => [
                'v' => '1',
                'cid' => 555,
                'tid' => 'testing',
                't' => 'pageview',
                'dh' => 'example.com',
                'dp' => '/home',
                'dt' => 'homepage',
            ],
        ]);
    }

    /** @test */
    public function it_handles_events()
    {
        $event = new Event('checkout', 'complete', 50.00, '');

        $provider = app(GoogleAnalytics::class)
            ->config(['id' => 'testing'])
            ->handle($event);

        $this->assertEquals($provider->lastOperation(), [
            'success' => true,
            'form_params' => [
                'v' => '1',
                'cid' => 555,
                'tid' => 'testing',
                't' => 'event',
                'ec' => 'checkout',
                'ea' => 'complete',
                'el' => '',
                'ev' => 50.00,
            ],
        ]);
    }

    /** @test */
    public function it_handles_transactions()
    {
        $transaction = new Transaction(12345, 'westernWear', 50.00, 32.00, 12.00, 'NOK');

        $provider = app(GoogleAnalytics::class)
            ->config(['id' => 'testing'])
            ->handle($transaction);

        $this->assertEquals($provider->lastOperation(), [
            'success' => true,
            'form_params' => [
                'v' => '1',
                'cid' => 555,
                'tid' => 'testing',
                't' => 'transaction',
                'ti' => 12345,
                'ta' => 'westernWear',
                'tr' => 50.0,
                'ts' => 32.0,
                'tt' => 12.0,
                'cu' => 'NOK',
            ],
        ]);
    }

    /** @test */
    public function it_handles_items()
    {
        $item = new Item(12345, 'sofa', 300, 2, 'u3eqds43', 'furniture', 'NOK');

        $provider = app(GoogleAnalytics::class)
            ->config(['id' => 'testing'])
            ->handle($item);

        $this->assertEquals($provider->lastOperation(), [
            'success' => true,
            'form_params' => [
                'v' => '1',
                'cid' => 555,
                'tid' => 'testing',
                't' => 'item',
                'in' => 'sofa',
                'ip' => 300,
                'iq' => 2,
                'ic' => 'u3eqds43',
                'iv' => 'furniture',
                'cu' => 'NOK',
            ],
        ]);
    }

    /** @test */
    public function throw_error_if_the_given_operation_is_not_supported()
    {
        try {
            $invalidOperation = new class {};

            $provider = app(GoogleAnalytics::class)
                ->config(['id' => 'testing'])
                ->handle($invalidOperation);
        } catch (\InvalidArgumentException $e) {
            $this->assertTrue(true);

            return;
        }

        $this->fail('Should Not Have Been A Valid Operation');
    }
}
