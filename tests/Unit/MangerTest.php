<?php

namespace Empatix\Analytics\Tests;

use Empatix\Analytics\Transaction;
use Empatix\Analytics\Drivers\Driver;
use Empatix\Analytics\Facades\Manager;
use Empatix\Analytics\Drivers\GoogleAnalytics;

class ManagerTest extends TestCase
{
    /** @test */
    public function it_has_default_config()
    {
        $this->assertEquals(Manager::config(), [
            'default' => 'google',
            'providers' => [
                'google' => [
                    'id' => 'testing',
                    'driver' => 'Empatix\Analytics\Drivers\GoogleAnalytics',
                ],
            ],
        ]);
    }

    /** @test */
    public function it_can_add_config()
    {
        Manager::config([
            'providers' => [
                'yahoo' => [
                    'id' => 'testing-2',
                    'driver' => 'App\Analytics\Drivers\YahooAnalytics',
                ],
            ],
        ]);

        $this->assertEquals(Manager::config(), [
            'default' => 'google',
            'providers' => [
                'google' => [
                    'id' => 'testing',
                    'driver' => 'Empatix\Analytics\Drivers\GoogleAnalytics',
                ],
                'yahoo' => [
                    'id' => 'testing-2',
                    'driver' => 'App\Analytics\Drivers\YahooAnalytics',
                ],
            ],
        ]);
    }

    /** @test */
    public function it_can_override_config()
    {
        Manager::config([
            'providers' => [
                'google' => [
                    'id' => 'testing-2',
                    'driver' => 'App\Analytics\Drivers\GoogleAnalytics',
                ],
            ],
        ]);

        $this->assertEquals(Manager::config(), [
            'default' => 'google',
            'providers' => [
                'google' => [
                    'id' => 'testing-2',
                    'driver' => 'App\Analytics\Drivers\GoogleAnalytics',
                ],
            ],
        ]);
    }

    /** @test */
    public function get_the_default_provider()
    {
        $provider = Manager::provider();
        $this->assertInstanceOf(Driver::class, $provider);
        $this->assertInstanceOf(GoogleAnalytics::class, $provider);
    }

    /** @test */
    public function it_register_a_given_operation()
    {
        $provider = Manager::register(new Transaction(12345, 'westernWear', 50.00, 32.00, 12.00, 'NOK'));

        // Assert last call equals...
        $this->assertTrue($provider->lastOperation()['success']);
    }
}
