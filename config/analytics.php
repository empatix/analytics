<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Analytics Provider.
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the analytics provider below you wish
    | to use as your default provider.
    |
    */

    'default' => env('ANALYTICS_PROVIDER', 'google'),

    /*
    |--------------------------------------------------------------------------
    | Analytics Providers.
    |--------------------------------------------------------------------------
    |
    | Here are each of the analytics providers setup for your application.
    |
    */

    'providers' => [

        'google' => [
            'id' => env('GOOGLE_ANALYTICS_ID'),
            'driver' => Empatix\Analytics\Drivers\GoogleAnalytics::class,
        ],

    ],

];
